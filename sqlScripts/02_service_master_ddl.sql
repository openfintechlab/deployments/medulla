alter session set current_schema = miscatalog;
DROP TABLE med_service;
CREATE TABLE med_service (
    srv_id              VARCHAR2(56 CHAR) NOT NULL,
    name                VARCHAR2(128 CHAR) NOT NULL,
    title               VARCHAR2(256 CHAR),
    group_slug          VARCHAR2(128 CHAR) NOT NULL,
    group_name          VARCHAR2(128 CHAR) NOT NULL,
    imp_tech_uri        VARCHAR2(256 CHAR) NOT NULL,
    source_uri          VARCHAR2(256 CHAR),
    hostname            VARCHAR2(128 CHAR) NOT NULL,
    port                NUMBER NOT NULL,
    ctx_root_uri        VARCHAR2(256 CHAR) NOT NULL,
    sdk_doc_uri         VARCHAR2(256 CHAR),
    api_doc_uri         VARCHAR2(256 CHAR),
    visibility          VARCHAR2(16 CHAR) NOT NULL,
    type                VARCHAR2(64 CHAR) NOT NULL,
    lifecycle           VARCHAR2(16 CHAR) NOT NULL,
    discussion_chl_uri  VARCHAR2(256 CHAR),
    tags                VARCHAR2(128 CHAR),
    doc_md_b64          CLOB,
    doc_md_file_name    VARCHAR2(256 CHAR),
    created_on          TIMESTAMP WITH TIME ZONE NOT NULL,
    updated_on          TIMESTAMP WITH TIME ZONE,
    created_by          VARCHAR2(64) NOT NULL,
    updated_by          VARCHAR2(64 CHAR) NOT NULL,
    last_ops_id         VARCHAR2(56 CHAR)
);

ALTER TABLE med_service ADD CONSTRAINT med_service_pk PRIMARY KEY ( srv_id );

ALTER TABLE med_service ADD CONSTRAINT med_service__un UNIQUE ( name );
exit;