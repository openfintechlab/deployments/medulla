alter session set current_schema = miscatalog;
CREATE TABLE med_service_versions (
    version_id           VARCHAR2(56 CHAR) NOT NULL,
    srv_id               VARCHAR2(56 CHAR) NOT NULL,
    description          VARCHAR2(128 BYTE) NOT NULL,
    container_reg_uri    VARCHAR2(256 CHAR) NOT NULL,
    container_reg_secid  VARCHAR2(16 CHAR),
    created_on           TIMESTAMP WITH TIME ZONE DEFAULT current_timestamp NOT NULL,
    updated_on           TIMESTAMP WITH TIME ZONE DEFAULT current_timestamp,
    created_by           VARCHAR2(64) NOT NULL,
    updated_by           VARCHAR2(64),
    last_ops_id          VARCHAR2(56 CHAR)
);

ALTER TABLE med_service_versions ADD CONSTRAINT versions_pk PRIMARY KEY ( version_id,
                                                                          srv_id );

ALTER TABLE med_service_versions
    ADD CONSTRAINT med_srv_ver_med_srv_fk FOREIGN KEY ( srv_id )
        REFERENCES med_service ( srv_id );
exit;        