alter session set current_schema = miscatalog;
DELETE FROM med_service_owner;
DROP TABLE med_service_owner;
CREATE TABLE med_service_owner (
    srv_own_id      VARCHAR2(56 CHAR) NOT NULL,
    srv_id          VARCHAR2(56 CHAR) NOT NULL,
    own_id          VARCHAR2(64 CHAR) NOT NULL,
    emailadd        VARCHAR2(128 CHAR) NOT NULL,
    escalationrank  NUMBER NOT NULL,
    created_on      TIMESTAMP WITH TIME ZONE DEFAULT current_timestamp NOT NULL,
    updated_on      TIMESTAMP WITH TIME ZONE DEFAULT current_timestamp NOT NULL,
    created_by      VARCHAR2(64 CHAR) NOT NULL,
    updated_by      VARCHAR2(64 CHAR) NOT NULL,
    last_ops_id     VARCHAR2(56 CHAR)
);

ALTER TABLE med_service_owner ADD CONSTRAINT med_service_owner_pk PRIMARY KEY ( srv_id,
                                                                                own_id );

ALTER TABLE med_service_owner ADD CONSTRAINT med_service_owner_un UNIQUE ( srv_own_id );

ALTER TABLE med_service_owner
    ADD CONSTRAINT med_srv_o_med_srv_fk FOREIGN KEY ( srv_id )
        REFERENCES med_service ( srv_id );

exit;        