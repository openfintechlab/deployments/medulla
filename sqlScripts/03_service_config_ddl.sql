alter session set current_schema = miscatalog;
CREATE TABLE med_service_config (
    service_id       VARCHAR2(56 CHAR) NOT NULL,
    service_name     VARCHAR2(128 CHAR) NOT NULL,
    version_id       VARCHAR2(56 CHAR) NOT NULL,
    config_key       VARCHAR2(256 CHAR) NOT NULL,
    config_key_desc  CLOB,
    config_value     CLOB,
    config_type      VARCHAR2(56 CHAR),
    config_hash      VARCHAR2(256 CHAR),
    created_on       TIMESTAMP WITH TIME ZONE,
    updated_on       TIMESTAMP WITH TIME ZONE,
    created_by       VARCHAR2(64 CHAR)
);

CREATE INDEX service_config__idx ON
    med_service_config (
        service_id
    ASC,
        version_id
    ASC,
        config_key
    ASC,
        config_type
    ASC,
        config_hash
    ASC );

ALTER TABLE med_service_config
    ADD CONSTRAINT service_config_pk PRIMARY KEY ( service_id,
                                                   version_id,
                                                   config_key );

exit;                                          