alter session set current_schema = miscatalog;
DROP TABLE application_address;
DROP TABLE application_owner;
DROP TABLE application_service_ep;
DROP TABLE med_service_application;

CREATE TABLE application_address (
    address_id      VARCHAR2(56 CHAR) NOT NULL,
    application_id  VARCHAR2(56 CHAR) NOT NULL,
    address_type    VARCHAR2(64 CHAR) NOT NULL,
    address         VARCHAR2(256 CHAR) NOT NULL,
    add_prop_uri    VARCHAR2(512 CHAR),
    created_on      TIMESTAMP(9) WITH TIME ZONE DEFAULT current_timestamp NOT NULL,
    updated_on      TIMESTAMP(9) WITH TIME ZONE DEFAULT current_timestamp,
    created_by      VARCHAR2(64 CHAR) NOT NULL,
    updated_by      VARCHAR2(64)
);

ALTER TABLE application_address ADD CONSTRAINT application_address_pk PRIMARY KEY ( address_id );

CREATE TABLE application_owner (
    application_id  VARCHAR2(56 CHAR) NOT NULL,
    own_id          VARCHAR2(64 CHAR) NOT NULL,
    emailadd        VARCHAR2(128 CHAR) NOT NULL,
    mobile_no       VARCHAR2(15 CHAR) NOT NULL,
    escalationrank  NUMBER NOT NULL,
    created_on      TIMESTAMP WITH TIME ZONE DEFAULT current_timestamp NOT NULL,
    updated_on      TIMESTAMP WITH TIME ZONE DEFAULT current_timestamp NOT NULL,
    created_by      VARCHAR2(64 CHAR) NOT NULL,
    updated_by      VARCHAR2(64 CHAR)
);

ALTER TABLE application_owner
    ADD CONSTRAINT med_service_ownerv1_pk PRIMARY KEY ( own_id,
                                                        application_id,
                                                        emailadd,
                                                        mobile_no );

CREATE TABLE application_service_ep (
    application_id  VARCHAR2(56 CHAR) NOT NULL,
    service_id      VARCHAR2(56 CHAR) NOT NULL,
    version_id      VARCHAR2(56 CHAR) NOT NULL,
    endpoint_id     VARCHAR2(56 CHAR) NOT NULL,
    created_on      TIMESTAMP(9) WITH TIME ZONE DEFAULT current_timestamp NOT NULL,
    updated_on      TIMESTAMP(9) WITH TIME ZONE DEFAULT current_timestamp NOT NULL,
    created_by      VARCHAR2(64),
    updated_by      VARCHAR2(56 CHAR)
);

ALTER TABLE application_service_ep
    ADD CONSTRAINT application_service_ep_pk PRIMARY KEY ( application_id,
                                                           service_id,
                                                           version_id,
                                                           endpoint_id );

CREATE TABLE med_service_application (
    application_id      VARCHAR2(56 CHAR) NOT NULL,
    name                VARCHAR2(128 CHAR) NOT NULL,
    status              VARCHAR2(4 CHAR) NOT NULL,
    status_description  VARCHAR2(64 CHAR),
    created_on          TIMESTAMP WITH LOCAL TIME ZONE DEFAULT current_timestamp NOT NULL,
    updated_on          TIMESTAMP(9) WITH LOCAL TIME ZONE DEFAULT current_timestamp,
    created_by          VARCHAR2(64 CHAR) NOT NULL,
    updated_by          VARCHAR2(56 CHAR)
);

ALTER TABLE med_service_application
    ADD CHECK ( status IN ( 'A', 'D', 'NV', 'S', 'V' ) );

ALTER TABLE med_service_application ADD CONSTRAINT med_service_application_pk PRIMARY KEY ( application_id );

ALTER TABLE med_service_application ADD CONSTRAINT med_service_application__un UNIQUE ( name );

ALTER TABLE application_address
    ADD CONSTRAINT app_add_med_srv_app_fk FOREIGN KEY ( application_id )
        REFERENCES med_service_application ( application_id )
            ON DELETE CASCADE;

ALTER TABLE application_owner
    ADD CONSTRAINT app_own_med_srv_app_fk FOREIGN KEY ( application_id )
        REFERENCES med_service_application ( application_id );

ALTER TABLE application_service_ep
    ADD CONSTRAINT app_ser_ep_med_ser_ep_fk FOREIGN KEY ( endpoint_id,
                                                          service_id,
                                                          version_id )
        REFERENCES med_service_endpoints ( endpoint_id,
                                           srv_id,
                                           version_id );

ALTER TABLE application_service_ep
    ADD CONSTRAINT app_srv_ep_med_service_app_fk FOREIGN KEY ( application_id )
        REFERENCES med_service_application ( application_id );
exit;        