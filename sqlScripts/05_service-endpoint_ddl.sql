alter session set current_schema = miscatalog;
CREATE TABLE med_service_endpoints (
    version_id    VARCHAR2(56 CHAR) NOT NULL,
    srv_id        VARCHAR2(56 CHAR) NOT NULL,
    endpoint_id   VARCHAR2(56 CHAR) NOT NULL,
    endpoint_uri  VARCHAR2(128 CHAR) NOT NULL,
    http_method   VARCHAR2(8 CHAR) NOT NULL,
    created_on    TIMESTAMP WITH TIME ZONE DEFAULT current_timestamp NOT NULL,
    updated_on    TIMESTAMP WITH TIME ZONE DEFAULT current_timestamp,
    created_by    VARCHAR2(64 CHAR) NOT NULL,
    updated_by    VARCHAR2(64 CHAR),
    last_ops_id   VARCHAR2(56 CHAR)
);

ALTER TABLE med_service_endpoints ADD CONSTRAINT med_service_endpoints_pk PRIMARY KEY ( endpoint_id );

ALTER TABLE med_service_endpoints
    ADD CONSTRAINT srv_ep_srv_ver_fk FOREIGN KEY ( version_id,
                                                   srv_id )
        REFERENCES med_service_versions ( version_id,
                                          srv_id );
exit;                                          