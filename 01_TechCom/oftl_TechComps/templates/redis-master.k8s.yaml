# Copyright 2020-21 Openfintechlab.com
#
# https://opensource.org/licenses/MIT
# 
# Permission is hereby granted, free of charge, to any person obtaining 
# a copy of this software and associated documentation files (the "Software"), 
# to deal in the Software without restriction, including without limitation 
# the rights to use, copy, modify, merge, publish, distribute, sublicense, 
# and/or sell copies of the Software, and to permit persons to whom the Software 
# is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all 
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
# PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
# CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
# OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

apiVersion: apps/v1 # for versions before 1.9.0 use apps/v1beta2
kind: Deployment
metadata: {{ $depID := uuidv4 }} # Deployment ID variable to be used in internal selectors
  name: {{.Values.RedisDeployment.name}}
  namespace: {{.Values.GenericVariables.namespace}}
  labels:
    app-uuid: {{ $depID }}
    openfintechlab.com/app: {{.Values.RedisDeployment.name}}
    # included from teamplate _labels.tpl    
    {{- include  "oftl.deployment.labels" . | nindent 4 }}
  annotations:
    openfintechlab.com/imageRegistry: {{.Values.RedisDeployment.imageRegistry | quote}}
spec:
  selector:
    matchLabels:
      app-uuid: {{ $depID }}
      openfintechlab.com/app: {{.Values.RedisDeployment.name}}      
  replicas: {{.Values.KeyCloakDeployment.replicaCount}}
  template:
    metadata:
      labels:
        app-uuid: {{ $depID }}
        openfintechlab.com/app: {{.Values.RedisDeployment.name}}
    spec:
      containers:
      - name: {{.Values.RedisDeployment.name}}-name
        # Changing to redis:6.0.8-buster from redis:alpine3.12
        image: "{{.Values.RedisDeployment.imageName}}:{{.Values.RedisDeployment.imageVersion}}"
        resources:
          requests:
            cpu: {{.Values.RedisDeployment.resourceRequest.cpu}}
            memory: {{.Values.RedisDeployment.resourceRequest.memory}}
        ports:
        {{- range $key, $value := .Values.RedisDeployment.ports}}
        - name: {{ $key }}
          containerPort: {{ $value }}        
        {{- end }}          
        args: 
        - "--appendonly"
        - "yes"
        volumeMounts:
          - name: {{.Values.RedisDeployment.localVol.name}}
            mountPath: {{.Values.RedisDeployment.localVol.contMountPath}}
      volumes:
      - name: {{.Values.RedisDeployment.localVol.name}}
        hostPath:
          # directory location on host
          path: {{.Values.RedisDeployment.localVol.hostPath}}
          # this field is optional
          type: {{.Values.RedisDeployment.localVol.type}}

{{ $runMode := .Values.runMode | default "prod" }}
{{- if eq $runMode  "dev" -}}
---
apiVersion: v1
kind: Service
metadata:
  creationTimestamp: null
  labels:
    app-uuid: {{ $depID }}
    openfintechlab.com/app: {{.Values.RedisDeployment.name}}
    # included from teamplate _labels.tpl    
    {{- include  "oftl.deployment.labels" . | nindent 4 }}
  name:  {{.Values.RedisDeployment.name}}-np
  namespace: {{.Values.GenericVariables.namespace}}
spec:
  ports:
  - nodePort: {{.Values.RedisDeployment.services.nodePort}}    
    protocol: TCP
    port: {{.Values.RedisDeployment.services.clusterPort}}  
    targetPort: {{.Values.RedisDeployment.ports.redisport}}
  selector:
    app-uuid: {{ $depID }}
    openfintechlab.com/app: {{.Values.RedisDeployment.name}}      
  type: NodePort
status:
  loadBalancer: {}    
{{ end }}

---
# application/guestbook/redis-master-service.yaml 
apiVersion: v1
kind: Service
metadata:
  name: {{.Values.RedisDeployment.name}}
  namespace: {{.Values.GenericVariables.namespace}}
  labels:
    app-uuid: {{ $depID }}
    openfintechlab.com/app: {{.Values.RedisDeployment.name}}
    # included from teamplate _labels.tpl    
    {{- include  "oftl.deployment.labels" . | nindent 4 }}
spec:
  type: ClusterIP
  ports:
  - port: {{.Values.RedisDeployment.services.clusterPort}}  
    targetPort: {{.Values.RedisDeployment.ports.redisport}}
  selector:
    app-uuid: {{ $depID }}
    openfintechlab.com/app: {{.Values.RedisDeployment.name}}      