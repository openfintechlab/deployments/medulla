# Introduction
Project for maintaining deployment scripts. Please refer to the specific branch for the versioned scripts

# Changelog

## [0.0.1-apha.01] - 2020-12-30
> **NOTE!** Changing build version to correspond with semantic version scheme (https://semver.org/) and also to corresponds with the JIRA releases


## [0.0.1_Alpha03] - 2020-12-27
### Service Componetns
1. medulla-redis-master   
2. configmanager          
3. service-owner          
4. usermanagement-kcl     
5. keycloak               
6. service-endpoints      
7. service-master         
8. service-version        
9. serviceconfigurationmanager
10. keycloak
11. docker/oracle11xg
12. binary/medctl
13. medmgmtconsole
14. Ngingx Ingress Controller
### Added
- medmgmtconsole
- Ingress Controller
### Changed
N/A
### Removed
n/a

## [0.0.1_Alpha02] - 2020-12-19
### Service Componetns
1. medulla-redis-master   
2. configmanager          
3. service-owner          
4. usermanagement-kcl     
5. keycloak               
6. service-endpoints      
7. service-master         
8. service-version        
9. serviceconfigurationmanager
10. keycloak
11. docker/oracle11xg
12. binary/medctl
### Added
- serviceconfigurationmanager
### Changed
- service-master
- service-version
- service-owner
- service-endpoints
- serivceconfigurationmanager
- deploy.sh
#### Table: MED_SERVICE_CONFIG
- Deleted Foreign key constraint and tuned the db for microservice
### Removed
n/a
