ip4=$(/sbin/ip -o -4 addr list $1 | awk '{print $4}' | cut -d/ -f1)

echo "Deploying service components"
helm upgrade --install --wait --timeout 120s  oftl-techcomps ./01_TechCom/oftl_TechComps -f ./01_TechCom/oftl_TechComps/values.yaml  --set DBHeadlessService.endpointIP=$ip4 --set runMode=dev