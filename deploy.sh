 #!/usr/bin/env bash

if [ -z "$1" ]
then
      echo "Usage: deploy.sh ens4"
      exit;
fi

ip4=$(/sbin/ip -o -4 addr list $1 | awk '{print $4}' | cut -d/ -f1)

echo '                    _       _ _       '
echo '                   | |     | | |      '
echo ' _ __ ___   ___  __| |_   _| | | __ _ '
echo '|  _ ` _ \ / _ \/ _` | | | | | |/ _` |'
echo '| | | | | |  __/ (_| | |_| | | | (_| |'
echo '|_| |_| |_|\___|\__,_|\__,_|_|_|\__,_|'
echo ''                                       
echo 'Copyright @ Openfintechlab.com'
echo 'Scripts for provisioning the solution infratructure'
echo 'Version: 0.0.1_Alpha02'                                      


###########################################
# START; 01_Technical Components
###########################################

echo '** Bootstarting Oracle Database server **'

# https://hub.docker.com/u/oracleinanutshell
# oracle Database
CONTAINER_NAME='oraclexe11g'
# Checking if docker container with $CONTAINER_NAME name exists.
COUNT=$(docker ps -a | grep "$CONTAINER_NAME" | wc -l)
if (($COUNT > 0)); then
    # cleanup
    echo '[-] Containers already running'
    if [ $2 == "--force" ]
    then
        docker container stop oraclexe11g
        docker container rm oraclexe11g
        docker image rm oracleinanutshell/oracle-xe-11g
    fi    
else
    # run your container
    echo '[+] Starting oracle container'
    docker image pull oracleinanutshell/oracle-xe-11g 
    docker run -d -p 49161:1521 -e ORACLE_ENABLE_XDB=true --restart unless-stopped --name oraclexe11g oracleinanutshell/oracle-xe-11g 
    # sleep for atleast 2 mins
    echo '[+] Waiting for the container to spin up'
    sleep 2m


    # RUN SQL statements for creating a user
    echo '[+] Copying SQL script files..'
    docker exec -it oraclexe11g /bin/sh -c 'rm -fr /sqlfiles && mkdir /sqlfiles'
    docker cp ./sqlScripts oraclexe11g:/sqlfiles

    ## Execute SQL statments in the container
    echo '[+] Executing scripts inside the database server container'
    docker exec -e ORACLE_HOME=/u01/app/oracle/product/11.2.0/xe -e ORACLE_SID=XE -it oraclexe11g /bin/sh -c '/u01/app/oracle/product/11.2.0/xe/bin/oracle_env.sh && /u01/app/oracle/product/11.2.0/xe/bin/sqlplus system/oracle @ /sqlfiles/sqlScripts/01_createUser.sql'
    docker exec -e ORACLE_HOME=/u01/app/oracle/product/11.2.0/xe -e ORACLE_SID=XE -it oraclexe11g /bin/sh -c '/u01/app/oracle/product/11.2.0/xe/bin/oracle_env.sh && /u01/app/oracle/product/11.2.0/xe/bin/sqlplus system/oracle @ /sqlfiles/sqlScripts/02_service_master_ddl.sql'
    docker exec -e ORACLE_HOME=/u01/app/oracle/product/11.2.0/xe -e ORACLE_SID=XE -it oraclexe11g /bin/sh -c '/u01/app/oracle/product/11.2.0/xe/bin/oracle_env.sh && /u01/app/oracle/product/11.2.0/xe/bin/sqlplus system/oracle @ /sqlfiles/sqlScripts/03_service_config_ddl.sql'
    docker exec -e ORACLE_HOME=/u01/app/oracle/product/11.2.0/xe -e ORACLE_SID=XE -it oraclexe11g /bin/sh -c '/u01/app/oracle/product/11.2.0/xe/bin/oracle_env.sh && /u01/app/oracle/product/11.2.0/xe/bin/sqlplus system/oracle @ /sqlfiles/sqlScripts/04_service-version_ddl.sql'
    docker exec -e ORACLE_HOME=/u01/app/oracle/product/11.2.0/xe -e ORACLE_SID=XE -it oraclexe11g /bin/sh -c '/u01/app/oracle/product/11.2.0/xe/bin/oracle_env.sh && /u01/app/oracle/product/11.2.0/xe/bin/sqlplus system/oracle @ /sqlfiles/sqlScripts/05_service-endpoint_ddl.sql'
    docker exec -e ORACLE_HOME=/u01/app/oracle/product/11.2.0/xe -e ORACLE_SID=XE -it oraclexe11g /bin/sh -c '/u01/app/oracle/product/11.2.0/xe/bin/oracle_env.sh && /u01/app/oracle/product/11.2.0/xe/bin/sqlplus system/oracle @ /sqlfiles/sqlScripts/06_service-owner_ddl.sql'
    docker exec -e ORACLE_HOME=/u01/app/oracle/product/11.2.0/xe -e ORACLE_SID=XE -it oraclexe11g /bin/sh -c '/u01/app/oracle/product/11.2.0/xe/bin/oracle_env.sh && /u01/app/oracle/product/11.2.0/xe/bin/sqlplus system/oracle @ /sqlfiles/sqlScripts/07_application_ddl.sql'
    docker exec -e ORACLE_HOME=/u01/app/oracle/product/11.2.0/xe -e ORACLE_SID=XE -it oraclexe11g /bin/sh -c '/u01/app/oracle/product/11.2.0/xe/bin/oracle_env.sh && /u01/app/oracle/product/11.2.0/xe/bin/sqlplus system/oracle @ /sqlfiles/sqlScripts/08_service_data.sql'

fi
# -----------------------------------
# Installing K8s technical components
# -----------------------------------
echo '** Installing Tech Layer Components **'
sudo rm -fr /k8svolumes/* && sudo mkdir -p /k8svolumes/.redis
sudo chown -R baqai:microk8s /k8svolumes/

kubectl config set-context --current --namespace=default
kubectl delete namespace medulla
echo '+ Uninstalling oftl-techcomps chart'
helm uninstall oftl-techcomps
echo '+ Installing oftl-techcomps chart. Waiting for 5mins for the cluster to stabilize'
helm upgrade --install --wait --timeout 600s oftl-techcomps ./01_TechCom/oftl_TechComps -f ./01_TechCom/oftl_TechComps/values.yaml --set DBHeadlessService.endpointIP=$ip4 --set runMode=dev

# -----------------------------------
# Installing K8s config components
# -----------------------------------
echo '** Installing Tech Layer Components and Loading intial Bootstart Config Data **'
echo '+ Installing oftl-configcomps. Waiting for 5mins for the cluster to stabilize'
helm upgrade --install --wait --timeout 600s oftl-configcomps ./02_ConfigComps/oftl_ConfigComps/ -f ./02_ConfigComps/oftl_ConfigComps/values.yaml --set runMode=dev

sudo cp ./binaries/* /usr/bin && sudo chmod +x /usr/bin/medctl

medctl -f ./02_ConfigComps/config/01_service-master-reg.yaml
medctl -f ./02_ConfigComps/config/02_service-owner-reg.yaml 
medctl -f ./02_ConfigComps/config/03_service-versions-reg.yaml 
medctl -f ./02_ConfigComps/config/04_service-endpoints-reg.yaml 
medctl -f ./02_ConfigComps/config/05_porcupine_usermanagement-kcl.yaml


# -----------------------------------
# Installing K8s kadalog components
# -----------------------------------
echo '** Installing kadalog components **'
echo '+ Installing oftl-kadalot. Waiting for 5mins for the cluster to stabilize'
helm install --wait --timeout 600s  oftl-kadalog  ./03_kadalog/oftl_Kadalog/ -f  ./03_kadalog/oftl_Kadalog/values.yaml  --set runMode=dev

echo '** INSTALLATION FINISHED **'

<<'###BLOCK-COMMENT'
# NO MORE NEEDED
# -----------------------------------
# Installing K8s config components
# -----------------------------------
echo '** Installing Tech Layer Components and Loading intial Bootstart Config Data **'
kubectl apply -k ./02_ConfigComps

sudo cp ./binaries/* /usr/bin && sudo chmod +x /usr/bin/medctl

# Wait to get the data sync and serviceconfigmanager to spin-up
sleep 2m

medctl -f ./02_ConfigComps/config/01_service-master-reg.yaml && \
medctl -f ./02_ConfigComps/config/02_service-owner-reg.yaml && \
medctl -f ./02_ConfigComps/config/03_service-versions-reg.yaml && \
medctl -f ./02_ConfigComps/config/04_service-endpoints-reg.yaml && \
medctl -f ./02_ConfigComps/config/05_porcupine_usermanagement-kcl.yaml


kubectl apply -k 03_kadalog

###########################################
# END;
###########################################



###BLOCK-COMMENT

# Reference
# https://github.com/ubuntu/microk8s/blob/master/microk8s-resources/actions/dns.yaml#L55