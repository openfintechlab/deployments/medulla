----------------------------------------------------------------------------------
** Installation of {{ .Chart.Name }} Finished **
----------------------------------------------------------------------------------
Important Configurations to Note:                                                                     
1. Deployment Mode: {{ .Values.deploymentmode }}  
2. Namespace: {{ .Values.namespace }} 
3. Service Config Manager Deployment [{{ .Values.ServiceConfigManagerDep.name }}]:  
    - Container Port: {{ .Values.ServiceConfigManagerDep.ports.http }} 
    - Node Port: {{ .Values.ServiceConfigManagerDep.services.nodePort }}  
    - Cluster Port: {{ .Values.ServiceConfigManagerDep.services.clusterPort }}  
    - Number of Replicas: {{ .Values.ServiceConfigManagerDep.replicaCount }} 
4. configuration Manager Deployment [{{ .Values.ConfigManagerDep.name }}]:     
    - Container Port: {{ .Values.ConfigManagerDep.ports.http }}  
    - Node Port: {{ .Values.ConfigManagerDep.services.nodePort }} 
    - Cluster Port: {{ .Values.ConfigManagerDep.services.clusterPort }} 
    - Number of Replicas: {{ .Values.ConfigManagerDep.replicaCount }}  
5. Config Loader Watch-Loop Controller: [{{ .Values.ConfigLoaderDep.name }}]  
   - Number of Replicas: {{ .Values.ConfigLoaderDep.replicaCount }} 
------------------------------------------------------------------------------------