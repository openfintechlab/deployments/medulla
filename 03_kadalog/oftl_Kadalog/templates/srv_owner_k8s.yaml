# Copyright 2020-21 Openfintechlab.com
#
# https://opensource.org/licenses/MIT
# 
# Permission is hereby granted, free of charge, to any person obtaining 
# a copy of this software and associated documentation files (the "Software"), 
# to deal in the Software without restriction, including without limitation 
# the rights to use, copy, modify, merge, publish, distribute, sublicense, 
# and/or sell copies of the Software, and to permit persons to whom the Software 
# is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all 
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
# PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
# CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
# OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

apiVersion: apps/v1
kind: Deployment
metadata: {{ $depID := uuidv4 }} # Deployment ID variable to be used in internal selectors
  creationTimestamp: null  
  name: {{.Values.ServiceOwnerDep.name}}
  namespace: {{.Values.GenericVariables.namespace}}
  labels:
    app-uuid: {{ $depID }}
    openfintechlab.com/app: {{.Values.ServiceOwnerDep.name}}
    # included from teamplate _labels.tpl    
    {{- include  "oftl.deployment.labels" . | nindent 4 }}  
spec:
  replicas: {{.Values.ServiceOwnerDep.replicaCount}}
  selector:
    matchLabels:
      app-uuid: {{ $depID }}
      openfintechlab.com/app: {{.Values.ServiceOwnerDep.name}}
  strategy: {}
  template:
    metadata:
      creationTimestamp: null
      labels:
        app-uuid: {{ $depID }}
        openfintechlab.com/app: {{.Values.ServiceOwnerDep.name}}
    spec:
      containers:      
      - image: "{{.Values.ServiceOwnerDep.imageName}}:{{.Values.ServiceOwnerDep.imageVersion}}"
        name: {{.Values.ServiceOwnerDep.name}}-cont    
        ports:
        {{- range $key, $value := .Values.ServiceOwnerDep.ports}}
        - name: {{ $key }}
          containerPort: {{ $value }}        
        {{- end }}    
        env:
        {{- range $key, $value := .Values.ServiceOwnerDep.environments}}             
        - name: {{ $key }}
          value: {{ $value | quote }}
        {{- end }}   
        resources: {}             
status: {}
{{ $runMode := .Values.runMode | default "prod" }}
{{- if eq $runMode  "dev" -}}
---
apiVersion: v1
kind: Service
metadata:  
  labels:
    app-uuid: {{ $depID }}
    openfintechlab.com/app: {{.Values.ServiceOwnerDep.name}}
  name: {{.Values.ServiceOwnerDep.name}}-np
  namespace: {{.Values.GenericVariables.namespace}}
spec:
  ports:
  - name: "kcl002-np"
    port: {{.Values.ServiceOwnerDep.ports.http}}
    protocol: TCP
    targetPort: {{.Values.ServiceOwnerDep.ports.http}}
    nodePort: {{.Values.ServiceOwnerDep.services.nodePort}}
  selector:
    app-uuid: {{ $depID }}
    openfintechlab.com/app: {{.Values.ServiceOwnerDep.name}}
  type: NodePort
status:
  loadBalancer: {}    

{{ end }}

---

apiVersion: v1
kind: Service
metadata:  
  labels:
    app-uuid: {{ $depID }}
    openfintechlab.com/app: {{.Values.ServiceOwnerDep.name}}
  name: {{.Values.ServiceOwnerDep.name}}
  namespace: {{.Values.GenericVariables.namespace}}
spec:
  ports:
  - name: "kcl002-cp"
    port: {{.Values.ServiceOwnerDep.services.clusterPort}}
    protocol: TCP
    targetPort: {{.Values.ServiceOwnerDep.ports.http}}
  selector:
    app-uuid: {{ $depID }}
    openfintechlab.com/app: {{.Values.ServiceOwnerDep.name}}
  type: ClusterIP
status:
  loadBalancer: {}      