# Generic deployment labels
{{- define "oftl.deployment.labels" -}}
openfintechlab.com/build-track: {{.Values.GenericVariables.deploymentmode}}
openfintechlab.com/mode: {{.Values.GenericVariables.buildtrack}}
openfintechlab.com/app-version: {{.Chart.AppVersion}}
openfintechlab.com/chart-version: {{.Chart.Version}}
openfintechlab.com/depID: {{.Values.deploymentID}}
openfintechlab.com/dep-date: {{ now | htmlDate }}
{{- end -}}